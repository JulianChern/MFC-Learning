先了解Win32窗口创建机制

## Win32创建窗口

创建窗口的步骤：（已创建好工程文件和主函数（winmain））
（1）	定义窗口类的结构体，并赋值  WNDCLASSEX wcex
（2）	向操作系统注册第1步中定义的窗口类 RegisterClassEx函数
（3）	通过注册的窗口类，创建具体的窗口 CreateWindow函数
（4）	显示并刷新上面的窗口ShowWindow和UpdateWindow函数
（5）	消息循环 GetMessage、TranslateMessage、DispatchMessage和窗口过程函数
### 解析VS自动生成的代码
**注册窗口类**
```cpp
//  函数: MyRegisterClass()
//  目的: 注册窗口类。
ATOM MyRegisterClass(HINSTANCE hInstance){
    ···
}
```
定义如下一个窗口类的结构体`wcex`并在`MyRegisterClass()`函数中注册：
```cpp
WNDCLASSEXW wcex;	//窗口类的结构体
wcex.cbSize = sizeof(WNDCLASSEX);		//多大

wcex.style          = CS_HREDRAW | CS_VREDRAW;	//窗口的类型
wcex.lpfnWndProc    = WndProc;					//关联窗口的窗口过程函数
wcex.cbClsExtra     = 0;
wcex.cbWndExtra     = 0;
wcex.hInstance      = hInstance;		//当前对象的实例
wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32APIDEMO));//导入图标
wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);//导入光标
wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);//窗口的背景
wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WIN32APIDEMO);//窗口的菜单
wcex.lpszClassName  = szWindowClass;//窗口的类名
wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//小图标

return RegisterClassExW(&wcex);//注册函数
```
**创建窗口**
```cpp
//   函数: InitInstance(HINSTANCE, int)
//   目的: 保存实例句柄并创建主窗口
//   注释: 
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow){
	hInst = hInstance; // 将实例句柄存储在全局变量中
    ···
	return TRUE;
}
```
创建主窗口`hWnd`
```cpp
HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

if (!hWnd)//验证窗口是否创建成功
{
return FALSE;
}
```
接着实现窗口显示与刷新：
```cpp
ShowWindow(hWnd, nCmdShow);//显示窗口
UpdateWindow(hWnd);//刷新窗口
```


**窗口过程函数**
```cpp
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//  目的:    处理主窗口的消息。
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//回调函数，窗口的句柄ID,与窗口相关联的消息
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 分析菜单选择: 
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT://绘图消息
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此处添加使用 hdc 的任何绘图代码...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY://窗口销毁时的消息
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);//将其他未处理的消息交由系统自动处理
	}
	return 0;
}
```


### 添加消息例子
在主窗口过程函数添加一个鼠标单击左键消息：
```cpp
case WM_LBUTTONDOWN:
{
    //消息框
    MessageBox(hWnd, _T("左键被按"), _T("标题"), MB_OK | MB_ICONERROR);
}
break;
```
## 消息机制
MFC添加消息响应三步骤：

* 头文件里声明
* 源文件添加消息映射(id绑定函数)
* 源文件消息响应函数


一键添加方式：右键添加事件处理程序OR直接双击控件OR通过类向导添加

例如为一个按钮添加消息响应事件获取编辑框的值：

**头文件声明**

```cpp
public:
	afx_msg void OnBnClickedButtonLogin();
```
**添加消息映射**
```cpp
BEGIN_MESSAGE_MAP(CDialogDemo1Dlg, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_LOGIN, &CDialogDemo1Dlg::OnBnClickedButtonLogin)//消息映射，id和函数通过宏相关联
END_MESSAGE_MAP()
```
**消息响应函数**

```cpp
void CDialogDemo1Dlg::OnBnClickedButtonLogin(){
	// TODO: 在此添加控件通知处理程序代码
	CString str;
	GetDlgItemText(IDC_EDIT_USERNAME, str);
	MessageBox(str);
}
```
## 模态对话框与非模态对话框
MFC创建模态对话框与非模态对话框：

<ol type="I">
    <li>主对话框添加按钮，并添加消息响应函数</li>
    <li>资源视图添加Dialog资源</li>
    <li>为该Dialog对话框添加CDialogEx的派生对话框类(ID关联)</li>
    <li>主对话框源文件添加派生对话框的头文件</li>
    <li>按钮消息响应函数里声明派生对话框类的对象来创建显示对话框</li>
</ol>

其中2、3步骤可合并，直接添加派生的对话框类然后赋予一个ID。

**模态对话框类的实例**
```cpp
CMODEL dlg;//声明一个模态对话框类的对象
dlg.DoModal();//显示模态对话框
```
**非模态对话框类的实例**
与模态对话框不同，非模态对话框类的实例应被声明为一个全局对象，否则对话框打开后会一闪而过。
主对话框类中声明一个非模态对话框对象
```cpp
private:
	CMODELESS *modelessDlg;//指针类型对象
```
按钮消息响应函数中：
```cpp
modelessDlg = new CMODELESS;//申请一段内存空间
modelessDlg->Create(IDD_DLG_MODELESS);//与非模态对话框ID相关联
modelessDlg->ShowWindow(SW_SHOW);//将对话框显示出来
```
声明为一个全局对象并动态分配内存后，应该考虑程序运行结束时该对象的释放。
在主对话框的析构函数delete该指针对象占用的内存：
```cpp
delete modelessDlg;
```

## 对话框传值
以主对话框计算a+b弹出结果对话框为例。
传值步骤：

* 添加结果对话框类CResult(可定义两个属性作为临时变量)。
* 主对话框中获取编辑框的值传给第一步中的临时变量。
* 给结果对话框类添加初始化函数，计算变量之和，初始化窗口的信息。

实现过程：
结果对话框类CResult.h添加属性作为临时变量：
```cpp
public:
	int tempVa1;
	int tempVa2;
```
为主对话框的编辑框添加公有变量：
```cpp
int m_firstVal;
int m_secondVal;
```
在主对话框的按钮消息响应函数里创建结果对话框类的实例，并对其属性传值：
```cpp
UpdateData(TRUE);//将控件的值写入到绑定的变量之中
	CResult dlg;
	dlg.tempVa1 = m_firstVal;
	dlg.tempVa2 = m_secondVal;
	dlg.DoModal();//显示模态对话框
```
为结果对话框类新建初始化窗口函数OnInitDialog()，并处理从主对话框接收到的两个值：
```cpp
m_Context.Format(_T("%d + %d = "), tempVa1, tempVa2);
m_Result = tempVa1 + tempVa2;
UpdateData(FALSE);
```
运行结果：
![](https://img.woyun.ink/wpimgs/article/2018/8/2/1533210613494.png)

## 双缓冲绘图
```cpp
CRect clientRect;//矩形区域大小类
GetClientRect(clientRect);//获取客户区大小

CImage imageBg;//声明一个图片对象
imageBg.Load(_T("img\\bg.jpeg"));

CPaintDC dc(this);//创建一个继承于CDC的设备环境类对象
CBitmap hBitMap;//创建内存中存放临时图像的位图对象
hBitMap.CreateCompatibleBitmap(&dc, clientRect.Width(), clientRect.Height());//创建与指定的设备环境相关的设备兼容的位图

CDC cMemDC;//创建用于缓冲作图的内存DC对象
cMemDC.CreateCompatibleDC(&dc);//依附窗口DC创建兼容内存DC(所有图形先画在这上面)

CBitmap* hOldBitMap = (CBitmap*)cMemDC.SelectObject(&hBitMap); //将位图选入内存DC

imageBg.BitBlt(cMemDC, 0, 0, clientRect.Width(), clientRect.Height(), 0, 0, SRCCOPY);//对指定的源设备环境区域中的像素进行位块转换
dc.BitBlt(0, 0, clientRect.Width(), clientRect.Height(), &cMemDC, 0, 0, SRCCOPY);//将内存DC上的图像复制到前台DC，即实际屏幕对象DC
cMemDC.SelectObject(hOldBitMap);//替换为原对象
hBitMap.DeleteObject();//释放对象
cMemDC.DeleteDC();//释放内存DC
```
## 定时器
三步骤：
启动定时器 setTimer函数
响应WM_TIMER消息
销毁定时器，killTimer函数
```cpp
void CTimerDemoDlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	SetTimer(ID_ME_TIMER, 1000, NULL);//设定一个定时器并启动
	SetDlgItemText(IDOK, _T("计时ing..."));
	//CDialogEx::OnOK();
}


void CTimerDemoDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch (nIDEvent)
	{
	case ID_ME_TIMER: {
		m_edt_Timer++;
		UpdateData(FALSE);
	}
					  break;
	default:
		break;
	}
	CDialogEx::OnTimer(nIDEvent);
}

void CTimerDemoDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	KillTimer(ID_ME_TIMER);
	CDialogEx::OnClose();
}

```

## 按键消息
常用函数：
`WM_KEYDOWN`
`PreTranslateMessage`
键值可参考文章：
1.<http://blog.csdn.net/witch_soya/article/details/6880419>
2.<http://www.cnblogs.com/huipengkankan/archive/2011/09/21/2184479.html>

## 列表视图
在初始化函数`OnInitDialog()`中对list control初始化
```cpp
CRect rect;
m_ListControl_UserInfo.GetClientRect(&rect);//获取控件大小，据此分配每一列的宽度
m_ListControl_UserInfo.SetExtendedStyle(m_ListControl_UserInfo.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);//设定样式
m_ListControl_UserInfo.InsertColumn(0, _T("姓名"), LVCFMT_CENTER, rect.Width()*0.5);//添加第1列
m_ListControl_UserInfo.InsertColumn(1, _T("学号"), LVCFMT_CENTER, rect.Width()*0.5);//添加第2列
```
添加内容
```cpp
m_ListControl_UserInfo.InsertItem(0, _T("张三"));
m_ListControl_UserInfo.SetItemText(0, 1, _T("16010001"));
m_ListControl_UserInfo.InsertItem(1, _T("李四"));
m_ListControl_UserInfo.SetItemText(1, 1, _T("16010021"));
```
在资源视图中选中列表视图并右键添加事件处理程序，下以获取选中列表文本为例：
```cpp
CString strUsrName;
NMLISTVIEW *pNMListView = (NMLISTVIEW*)pNMHDR;//是NMHDR的派生类（结构体）之一，它专用于LISTVIEW的WM_NOTIFY消息
if (-1 != pNMListView->iItem) {//如果iItem不是-1表明列表被选中
	strUsrName = m_ListControl_UserInfo.GetItemText(pNMListView->iItem, 0);//获取被选择列表项第一个子项的文本   
	MessageBox(strUsrName);
}
```
删除内容(参考<https://blog.csdn.net/technologyleader/article/details/80846100>)
```cpp
for (int iItem = m_ListControl_UserInfo.GetItemCount() - 1; iItem >= 0; iItem--) {
	if (m_ListControl_UserInfo.GetItemState(iItem, LVIS_SELECTED) == LVIS_SELECTED) {
		m_ListControl_UserInfo.DeleteItem(iItem);
	}
}
```

## 百度地图
此处有参考<https://blog.csdn.net/techtiger/article/details/14155627>

右键插入ActiveX控件->Microsoft Web Browser
新建一个ActiveX控件中的MFC类：

* 可用的ActiveX控件：Microsoft Web Browser<1.0>
* 接口：IWebBrowser2

添加`CWebBrowser2.h`头文件
右键控件添加CWebBrowser2类型变量`m_ie`
在`OnInitDialog()`中初始化：
```
//百度地图
//CString str("D:\\001Download\\map.html");
TCHAR szPath[_MAX_PATH];
GetCurrentDirectory(_MAX_PATH, szPath);
CString str = szPath;
str += _T("\\res\\map.html");
m_ie.Navigate(str, NULL, NULL, NULL,NULL);
```
另：
若出现报错：`DDX_Control`: 函数不接受 4 个参数。
删掉`DISPID()`即可。

查找位置：
添加头文件`MsHTML.h`
```cpp
void CBaiduMapDemoDlg::OnBnClickedBtnShowpoint()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	//调用浏览器相关的内容
	CComQIPtr<IHTMLDocument2>pDoc = m_ie.get_Document();//智能指针
	CComDispatchDriver spPrict;
	pDoc->get_Script(&spPrict);//调用函数与js代码相关联

	CComVariant varLongitude, varLatitude, res;
	varLongitude = m_longitude;
	varLatitude = m_latitude;

	spPrict.Invoke2(L"theFun2", &varLongitude, &varLatitude, &res);
	
}
```
导航：
```cpp
void CBaiduMapDemoDlg::OnBnClickedBtnGo()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	//调用浏览器相关的内容
	CComQIPtr<IHTMLDocument2>pDoc = m_ie.get_Document();//智能指针
	CComDispatchDriver spPrict;
	pDoc->get_Script(&spPrict);//调用函数与js代码相关联

	CComVariant varStart, varEnd, res;
	varStart = m_Start;
	varEnd = m_End;
	
	//spPrict.Invoke2(L"theFun3", &varStart, &varEnd, &res);
	spPrict.Invoke2(L"theFun4", &varStart, &varEnd, &res);

}
```
