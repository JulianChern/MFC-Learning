
// MFCFileOpDemoDlg.cpp: 实现文件
//
#include "stdafx.h"

#include "MFCFileOpDemo.h"
#include "MFCFileOpDemoDlg.h"
#include "afxdialogex.h"

#include <atlconv.h>//字符串转换宏所包含的头文件
#include "CUser.h"

#pragma warning(disable : 4996)

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCFileOpDemoDlg 对话框



CMFCFileOpDemoDlg::CMFCFileOpDemoDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCFILEOPDEMO_DIALOG, pParent)
	, m_edt_UserName(_T(""))
	, m_edt_Pwd(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCFileOpDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDT_USERNAME, m_edt_UserName);
	DDX_Text(pDX, IDC_EDT_PASSWORD, m_edt_Pwd);
}

BEGIN_MESSAGE_MAP(CMFCFileOpDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_WRITE, &CMFCFileOpDemoDlg::OnBnClickedBtnWrite)
	ON_BN_CLICKED(IDC_BTN_READ, &CMFCFileOpDemoDlg::OnBnClickedBtnRead)
END_MESSAGE_MAP()


// CMFCFileOpDemoDlg 消息处理程序

BOOL CMFCFileOpDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCFileOpDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCFileOpDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCFileOpDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



//写，将一个CUser类对象直接写进去。CFile类
void CMFCFileOpDemoDlg::OnBnClickedBtnWrite()
{
	// TODO: 在此添加控件通知处理程序代码

	//MFC的文件操作是CFile类来实现的
	UpdateData(TRUE);//得到控件的值

	USES_CONVERSION;//类似于提前声明
	//进行字符串转换
	CUser userInfo;
	strcpy(userInfo.name,T2A(m_edt_UserName));
	strcpy(userInfo.pwd, T2A(m_edt_Pwd));

	CFile file;//文件对象
	//这个文件名路径为相对路径,表示这个文件在当前程序文件夹里
	file.Open(_T("1.txt"), CFile::modeWrite | CFile::modeCreate | CFile::modeNoTruncate | CFile::shareExclusive);
	file.SeekToEnd();//将文件指针移动至文件末尾
	file.Write(&userInfo, sizeof(CUser));
	file.Close();
}


//读
void CMFCFileOpDemoDlg::OnBnClickedBtnRead()
{
	// TODO: 在此添加控件通知处理程序代码
	CUser* userInfo = new CUser;
	CFile file;
	file.Open(_T("1.txt"), CFile::modeRead | CFile::shareExclusive);
	int nSize = file.GetLength();//得到文件大小
	int i = 0;
	CString strName;
	CString strPwd;

	//得到文件的指针位置，与文件大小比较，判断是否到了文件末尾
	while (file.GetPosition() <= nSize) {
		//读出来的文件进行加工处理
		file.Read(&userInfo[i],sizeof(CUser));
		file.Seek(sizeof(CUser)*(i+1),CFile::begin);
		//处理
		USES_CONVERSION;
		strName = A2T(userInfo[i].name);
		strPwd = A2T(userInfo[i].pwd);
		i++;
	}
	file.Close();
}
