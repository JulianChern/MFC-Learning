
// MFCFileOpDemoDlg.h: 头文件
//

#pragma once


// CMFCFileOpDemoDlg 对话框
class CMFCFileOpDemoDlg : public CDialogEx
{
// 构造
public:
	CMFCFileOpDemoDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCFILEOPDEMO_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// 用户名
	CString m_edt_UserName;
	// 密码
	CString m_edt_Pwd;
	afx_msg void OnBnClickedBtnWrite();
	afx_msg void OnBnClickedBtnRead();
};
