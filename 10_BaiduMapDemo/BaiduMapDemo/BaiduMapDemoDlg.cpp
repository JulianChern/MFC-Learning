
// BaiduMapDemoDlg.cpp: 实现文件
//

#include "stdafx.h"
#include "BaiduMapDemo.h"
#include "BaiduMapDemoDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include"MsHTML.h"

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CBaiduMapDemoDlg 对话框



CBaiduMapDemoDlg::CBaiduMapDemoDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_BAIDUMAPDEMO_DIALOG, pParent)
	, m_longitude(_T(""))
	, m_latitude(_T(""))
	, m_Start(_T(""))
	, m_End(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CBaiduMapDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER1, m_ie);
	//  DDX_Control(pDX, IDC_EDIT1, m_longtitude);
	//  DDX_Control(pDX, IDC_EDIT2, m_latitude);
	DDX_Text(pDX, IDC_EDT_LONGITUDE, m_longitude);
	DDX_Text(pDX, IDC_EDT_LATITUDE, m_latitude);
	DDX_Text(pDX, IDC_EDT_START, m_Start);
	DDX_Text(pDX, IDC_EDT_END, m_End);
}

BEGIN_MESSAGE_MAP(CBaiduMapDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_SHOWPOINT, &CBaiduMapDemoDlg::OnBnClickedBtnShowpoint)
	ON_BN_CLICKED(IDC_BTN_GO, &CBaiduMapDemoDlg::OnBnClickedBtnGo)
END_MESSAGE_MAP()


// CBaiduMapDemoDlg 消息处理程序

BOOL CBaiduMapDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	//百度地图
	//CString str("D:\\001Download\\map.html");
	TCHAR szPath[_MAX_PATH];
	GetCurrentDirectory(_MAX_PATH, szPath);
	CString str = szPath;
	str += _T("\\res\\map1.html");
	m_ie.Navigate(str, NULL, NULL, NULL, NULL);


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CBaiduMapDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CBaiduMapDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CBaiduMapDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CBaiduMapDemoDlg::OnBnClickedBtnShowpoint()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	//调用浏览器相关的内容
	CComQIPtr<IHTMLDocument2>pDoc = m_ie.get_Document();//智能指针
	CComDispatchDriver spPrict;
	pDoc->get_Script(&spPrict);//调用函数与js代码相关联

	CComVariant varLongitude, varLatitude, res;
	varLongitude = m_longitude;
	varLatitude = m_latitude;

	spPrict.Invoke2(L"theFun2", &varLongitude, &varLatitude, &res);

}


void CBaiduMapDemoDlg::OnBnClickedBtnGo()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	//调用浏览器相关的内容
	CComQIPtr<IHTMLDocument2>pDoc = m_ie.get_Document();//智能指针
	CComDispatchDriver spPrict;
	pDoc->get_Script(&spPrict);//调用函数与js代码相关联

	CComVariant varStart, varEnd, res;
	varStart = m_Start;
	varEnd = m_End;

	//spPrict.Invoke2(L"theFun3", &varStart, &varEnd, &res);
	spPrict.Invoke2(L"theFun4", &varStart, &varEnd, &res);

}
