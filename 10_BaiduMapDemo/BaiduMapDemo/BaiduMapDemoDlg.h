
// BaiduMapDemoDlg.h: 头文件
//

#pragma once
#include"CWebBrowser2.h"


// CBaiduMapDemoDlg 对话框
class CBaiduMapDemoDlg : public CDialogEx
{
// 构造
public:
	CBaiduMapDemoDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BAIDUMAPDEMO_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// 浏览器
	CWebBrowser2 m_ie;
	// 经度// 经度
	CString m_longitude;
	// 纬度
	CString m_latitude;
	afx_msg void OnBnClickedBtnShowpoint();
	// 起始地
	CString m_Start;
	// 目的地
	CString m_End;
	afx_msg void OnBnClickedBtnGo();
};
