#pragma once


// CMODEL 对话框

class CMODEL : public CDialogEx
{
	DECLARE_DYNAMIC(CMODEL)

public:
	CMODEL(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CMODEL();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_MODEL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
