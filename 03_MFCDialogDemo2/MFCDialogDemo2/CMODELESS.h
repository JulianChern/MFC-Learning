#pragma once


// CMODELESS 对话框

class CMODELESS : public CDialogEx
{
	DECLARE_DYNAMIC(CMODELESS)

public:
	CMODELESS(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CMODELESS();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_MODELESS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
