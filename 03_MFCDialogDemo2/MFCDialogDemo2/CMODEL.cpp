// CMODEL.cpp: 实现文件
//

#include "stdafx.h"
#include "MFCDialogDemo2.h"
#include "CMODEL.h"
#include "afxdialogex.h"


// CMODEL 对话框

IMPLEMENT_DYNAMIC(CMODEL, CDialogEx)

CMODEL::CMODEL(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_MODEL, pParent)
{

}

CMODEL::~CMODEL()
{
}

void CMODEL::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMODEL, CDialogEx)
END_MESSAGE_MAP()


// CMODEL 消息处理程序
