// CMODELESS.cpp: 实现文件
//

#include "stdafx.h"
#include "MFCDialogDemo2.h"
#include "CMODELESS.h"
#include "afxdialogex.h"


// CMODELESS 对话框

IMPLEMENT_DYNAMIC(CMODELESS, CDialogEx)

CMODELESS::CMODELESS(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DLG_MODELESS, pParent)
{

}

CMODELESS::~CMODELESS()
{
}

void CMODELESS::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMODELESS, CDialogEx)
END_MESSAGE_MAP()


// CMODELESS 消息处理程序
