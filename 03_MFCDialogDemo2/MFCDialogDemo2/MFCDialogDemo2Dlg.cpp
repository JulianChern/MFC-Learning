
// MFCDialogDemo2Dlg.cpp: 实现文件
//

#include "stdafx.h"
#include "MFCDialogDemo2.h"
#include "MFCDialogDemo2Dlg.h"
#include "afxdialogex.h"

//对话框
#include"CMODEL.h"//关联模态对话框头文件

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCDialogDemo2Dlg 对话框



CMFCDialogDemo2Dlg::CMFCDialogDemo2Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCDIALOGDEMO2_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

//析构函数
CMFCDialogDemo2Dlg::~CMFCDialogDemo2Dlg() {
	delete modelessDlg;
}

void CMFCDialogDemo2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMFCDialogDemo2Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_MODEL, &CMFCDialogDemo2Dlg::OnBnClickedBtnModel)
	ON_BN_CLICKED(IDC_BTN_MODELESS, &CMFCDialogDemo2Dlg::OnBnClickedBtnModeless)
END_MESSAGE_MAP()


// CMFCDialogDemo2Dlg 消息处理程序

BOOL CMFCDialogDemo2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCDialogDemo2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCDialogDemo2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCDialogDemo2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


/*
双击、类向导添加
*/
//用来显示模态对话框
void CMFCDialogDemo2Dlg::OnBnClickedBtnModel()
{
	// TODO: 在此添加控件通知处理程序代码
	//单击显示模态对话框
	CMODEL dlg;//声明一个模态对话框类的对象
	dlg.DoModal();//显示模态对话框
}


//用来显示非模态对话框
void CMFCDialogDemo2Dlg::OnBnClickedBtnModeless()
{
	// TODO: 在此添加控件通知处理程序代码
	/*
	局部变量，当前函数运行结束时，局部变量会销毁，所以窗口一闪而过。
	解决办法：申请一个内存空间
	CMODELESS dlg;//局部变量
	dlg.Create(IDD_DLG_MODELESS);//与非模态对话框ID相关联
	dlg.ShowWindow(SW_SHOW);//将对话框显示出来
	*/
	//申请的空间需要释放
	modelessDlg = new CMODELESS;//申请一段内存空间
	modelessDlg->Create(IDD_DLG_MODELESS);//与非模态对话框ID相关联
	modelessDlg->ShowWindow(SW_SHOW);//将对话框显示出来
}






