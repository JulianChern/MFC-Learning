
// MFCDialogDemo2Dlg.h: 头文件
//


#include"CMODELESS.h"//关联非模态对话框文件

#pragma once


// CMFCDialogDemo2Dlg 对话框
class CMFCDialogDemo2Dlg : public CDialogEx
{
// 构造
public:
	CMFCDialogDemo2Dlg(CWnd* pParent = nullptr);	// 标准构造函数
	//析构函数
	~CMFCDialogDemo2Dlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCDIALOGDEMO2_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

private:
	CMODELESS *modelessDlg;//指针类型对象

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnModel();
	afx_msg void OnBnClickedBtnModeless();
};
