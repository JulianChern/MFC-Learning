//Win32APIDemo.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "Win32APIDemo.h"

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);	//注册类
BOOL                InitInstance(HINSTANCE, int);			//初始化当前对象的实例
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);	//主窗口过程函数
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);		//关于窗口的窗口过程函数

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,//当前对象的实例
	_In_opt_ HINSTANCE hPrevInstance,//通常为NULL
	_In_ LPWSTR    lpCmdLine,			//参数的行数
	_In_ int       nCmdShow)			//窗口的显示相关设置
{
	UNREFERENCED_PARAMETER(hPrevInstance);//作用就是避免这个变量在未使用的情况下发出警告
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 在此放置代码。

	// 初始化全局字符串
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_WIN32APIDEMO, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// 执行应用程序初始化: 
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WIN32APIDEMO));

	MSG msg;

	// 主消息循环: 
	while (GetMessage(&msg, nullptr, 0, 0))//获取消息
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);//编译消息，翻译
			DispatchMessage(&msg);//分发消息
		}
	}

	return (int)msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;	//窗口类的结构体

	wcex.cbSize = sizeof(WNDCLASSEX);		//多大

	wcex.style = CS_HREDRAW | CS_VREDRAW;	//窗口的类型
	wcex.lpfnWndProc = WndProc;					//关联窗口的窗口过程函数
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;		//当前对象的实例
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32APIDEMO));//导入图标
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);//导入光标
	//wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);//窗口的背景
	wcex.hbrBackground = (HBRUSH)CreateSolidBrush(RGB(0, 0, 0));//窗口的背景，画刷
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_WIN32APIDEMO);//窗口的菜单
	wcex.lpszClassName = szWindowClass;//窗口的类名
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//小图标

	return RegisterClassExW(&wcex);//注册函数
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // 将实例句柄存储在全局变量中

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
	HWND hWnd1 = CreateWindowEx(0, szWindowClass, _T("liksi"), WS_OVERLAPPEDWINDOW, 100, 100, 200, 200, NULL, NULL, hInstance, NULL);


	if (!hWnd)//验证窗口是否创建成功
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);//显示窗口
	UpdateWindow(hWnd);//刷新窗口

	ShowWindow(hWnd1, nCmdShow);//显示窗口
	UpdateWindow(hWnd1);//刷新窗口

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//回调函数
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// 分析菜单选择: 
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT://绘图消息
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此处添加使用 hdc 的任何绘图代码...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY://窗口销毁时的消息
		PostQuitMessage(0);
		break;

	case WM_LBUTTONDOWN:
	{
		//消息框
		//MessageBox(hWnd,_T("左键被按"),_T("标题"),MB_YESNOCANCEL|MB_ICONINFORMATION);
		MessageBox(hWnd, _T("左键被按"), _T("标题"), MB_OK | MB_ICONERROR);
	}
	break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);//将其他未处理的消息交由系统自动处理
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
