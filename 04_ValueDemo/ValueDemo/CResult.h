#pragma once


// CResult 对话框

class CResult : public CDialogEx
{
	DECLARE_DYNAMIC(CResult)

public:
	CResult(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CResult();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

public:
	int tempVa1;
	int tempVa2;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	// //静态文本框
	CString m_Context;
	// //求和的值
	int m_Result;
	virtual BOOL OnInitDialog();
};
