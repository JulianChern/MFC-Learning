// CResult.cpp: 实现文件
//

#include "stdafx.h"
#include "ValueDemo.h"
#include "CResult.h"
#include "afxdialogex.h"


// CResult 对话框

IMPLEMENT_DYNAMIC(CResult, CDialogEx)

CResult::CResult(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
	, m_Context(_T(""))
	, m_Result(0)
{

}

CResult::~CResult()
{
}

void CResult::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_STATIC_CONTEXT, m_Context);
	DDX_Text(pDX, IDC_EDT_RESULT, m_Result);
}


BEGIN_MESSAGE_MAP(CResult, CDialogEx)
END_MESSAGE_MAP()


// CResult 消息处理程序


BOOL CResult::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	//两个临时变量有值
	m_Context.Format(_T("%d + %d = "), tempVa1, tempVa2);
	m_Result = tempVa1 + tempVa2;
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
