
// DialogDemo1Dlg.h: 头文件
//

#pragma once


// CDialogDemo1Dlg 对话框
class CDialogDemo1Dlg : public CDialogEx
{
// 构造
public:
	CDialogDemo1Dlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOGDEMO1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	/*
	添加响应机制需要三步：
	头文件里声明->源文件添加消息映射(id绑定函数)->源文件消息相应函数
	*/
	afx_msg void OnBnClickedButtonLogin();//声明
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};
