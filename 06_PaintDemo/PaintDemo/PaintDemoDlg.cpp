
// PaintDemoDlg.cpp: 实现文件
//

#include "stdafx.h"
#include "PaintDemo.h"
#include "PaintDemoDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPaintDemoDlg 对话框



CPaintDemoDlg::CPaintDemoDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PAINTDEMO_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPaintDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPaintDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()


// CPaintDemoDlg 消息处理程序

BOOL CPaintDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CPaintDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

//像响应的是WM_PAINT消息
void CPaintDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//DC叫做设备上下文/设备描述表
		CPaintDC dc(this);//创建一个绘图ds//继承于CDC
		CImage imageBg;//声明一个图片对象
		imageBg.Load(_T("img\\bg.jpeg"));
		CRect clientRect;//区域大小类
		GetClientRect(&clientRect);//获取客户区大小

		//imageBg.Draw(dc, 0, 0, clientRect.Width(), clientRect.Height());
		//双缓冲绘图:先将图片画在内存dc上，然后将内存dc的消息画在当前dc窗口上
		CImage imageSwore;
		imageSwore.Load(_T("img\\jian.png"));
		CDC cMemDC;//内存DC
		CBitmap hBitMap;//位图对象，bmp格式
		hBitMap.CreateCompatibleBitmap(&dc, clientRect.Width(), clientRect.Height());//创建一个相兼容的位图对象
		cMemDC.CreateCompatibleDC(&dc);//创建一个相兼容的dc
		CBitmap* hOldBitMap = (CBitmap*)cMemDC.SelectObject(&hBitMap);//保存之前的对象
		cMemDC.FillSolidRect(clientRect, RGB(255, 255, 255));
		//先在内存DC中画
		imageBg.BitBlt(cMemDC, 0, 0, clientRect.Width(), clientRect.Height(), 0, 0, SRCCOPY);
		imageSwore.BitBlt(cMemDC, 50, 50, 48, 48, 0, 0, SRCCOPY);

		dc.BitBlt(0, 0, clientRect.Width(), clientRect.Height(), &cMemDC, 0, 0, SRCCOPY);
		cMemDC.SelectObject(hOldBitMap);//选出对象还原
		hBitMap.DeleteObject();//释放对象
		cMemDC.DeleteDC();//释放dc

		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CPaintDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

