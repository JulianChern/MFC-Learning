
// ListControlDemoDlg.h: 头文件
//

#pragma once


// CListControlDemoDlg 对话框
class CListControlDemoDlg : public CDialogEx
{
// 构造
public:
	CListControlDemoDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LISTCONTROLDEMO_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// 列表视图控件
	CListCtrl m_ListControl_UserInfo;
	afx_msg void OnBnClickedBtnAdd();
	// 姓名
	CString m_edt_name;
	// 学号
	int m_edt_num;
	//列表序号
	int iNum;
	afx_msg void OnNMClickLstCtrl(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedBtnDel();
};
